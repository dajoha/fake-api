
const express = require('express')
const json = express.json();

const app = express();
const port = 3000;

const { JsonError } = require('./JsonError');

const loremIpsum = require('./Lorem');
const Lorem = loremIpsum.Lorem;
loremGenerator = new Lorem();


// Main route:
app.get('/', json, (req, res, next) => {
    const body = req.body;

    // Generate the main message depending on the request:
    let messageString = '';
    if (typeof body.title === 'string') {
        messageString = body.title;
    } else if (typeof body.lorem === 'object') {
        const lorem = body.lorem;
        if (typeof lorem.count !== 'number' || !loremIpsum.types.includes(lorem.type)) {
            throw new JsonError(400, 'Lorem ipsum: bad request');
        }
        messageString = loremGenerator.generate(lorem.count, lorem.type);
    }

    // Send the response at the right moment:
    const time = typeof body.time === 'number' ? body.time : 0;
    setTimeout(() => {
        if (req.accepts('json')) {
            res.data = { message: messageString };
        } else {
            res.data = messageString;
        }
        next();
    }, time);
})

// Handle the response format:
app.use((req, res, next) => {
    if (res.data === undefined) {
        next(new JsonError(404, 'Not found'));
    }
    let data = res.data;
    if (req.accepts('json')) {
        const message = { success: true };
        if (typeof data === 'object' && !Array.isArray(data)) {
            Object.assign(message, data);
        } else {
            message.data = data;
        }
        res.json(message);
    } else {
        if (typeof data === 'object') {
            data = JSON.stringify(data);
        }
        res.type('text').send(data);
    }
});

// Handle errors:
app.use((err, req, res, next) => {
    if (err instanceof JsonError) {
        res
            .status(err.statusCode)
            .json(err.getJson());
    } else {
        next(err);
    }
});

// Run the server:
app.listen(port, () => console.log(`Listening on port ${port}...`))
