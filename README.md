
fakeapi v0.1
============

INSTALLATION
============

Run `yarn install` in the root directory.

RUNNING
=======

Run `node src` in the root directory.

CONFIGURATION
=============

No configuration... The static host is `localhost:3000`.

API REFERENCE
=============

The only route is `/`, with one mandatory header:

    Content-Type: application/json

The body must be a json object with the following format:

    {
        "title": "Hi",    // Optional: the given title will be shown in the response
        "lorem": {        // Optional: some lorem ipsum will be generated in the response
            "count": 32,                       // Number of words, sentences or paragraphs to
                                               // generate
            "type": "word|sentence|paragraph"  // Type of block to generate
        },
        "time": 2000      // Delay to wait before sending the response; default: 0ms
    }

If "title" is given, it will override "lorem".

The api accepts the following extra header:

    Accept: text/plain

If this header is given, the final message will be given raw, either the given title or the lorem
ipsum generated text.
Otherwise, the message will be a json object of the following format:

    {
        "success": true|false,
        "message": ...
    }

